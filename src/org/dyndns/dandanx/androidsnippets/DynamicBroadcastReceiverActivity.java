package org.dyndns.dandanx.androidsnippets;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

/**
 * Register for broadcast intents from AndroidMediaPlayer and sends it to Android Remote Notifier 
 * @author dandanx
 */
public class DynamicBroadcastReceiverActivity extends Activity {

	public static final String TAG = DynamicBroadcastReceiverActivity.class.getSimpleName();
	
	public static BroadcastReceiver bcReciver = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Display some informations
		IntentFilter intentFilter1 = new IntentFilter("com.android.music.metachanged");

		if (bcReciver == null) {
			bcReciver = new BroadcastReceiver() {
				public void onReceive(Context context, Intent intent) {

					//Display some informations
					if (intent.getAction() != null) {
						Log.i(TAG, "Action: " + intent.getAction());
					}
					if (intent.getExtras() != null) {
						
						String artist = "";
						String track = "";
						
						for (String key : intent.getExtras().keySet()) {
							Log.i(TAG, key + ": " + intent.getExtras().get(key).toString());
							
							if(key.contains("artist")) artist = intent.getExtras().get(key).toString();
							if(key.contains("track")) track = intent.getExtras().get(key).toString();
						}
						
						Bundle b = intent.getExtras();
						sendRemoteNotification(getResources().getString(R.string.dbra_now_playing), artist + " - " + track);
					}
				}
			};
		}

		registerReceiver(bcReciver, intentFilter1);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		unregisterReceiver(bcReciver);
	}
	/**
	 * Sends a BroadcastIntent to Remote Notifier
	 * @see http://code.google.com/p/android-notifier/
	 * @see http://code.google.com/p/android-notifier/wiki/ThirdPartyNotifications
	 */
	private void sendRemoteNotification(String title, String description) {
		Intent i = new Intent("org.damazio.notifier.service.UserReceiver.USER_MESSAGE");
        i.putExtra("title", title);
        i.putExtra("description", description);
        sendBroadcast(i);
	}
}