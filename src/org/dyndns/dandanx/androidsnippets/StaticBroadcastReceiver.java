package org.dyndns.dandanx.androidsnippets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Receives broadcast intents from several music apps and sends it to Android
 * Remote Notifier
 * 
 * @author dandanx
 */
public class StaticBroadcastReceiver extends BroadcastReceiver {

	public static final String TAG = StaticBroadcastReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {

		// Display some informations
		if (intent.getAction() != null) {
			Log.i(TAG, "Action: " + intent.getAction());
		}
		if (intent.getExtras() != null) {
			String artist = "";
			String track = "";
			
			for (String key : intent.getExtras().keySet()) {
				Log.i(TAG, key + ": " + intent.getExtras().get(key).toString());
				
				if(key.contains("artist")) artist = intent.getExtras().get(key).toString();
				if(key.contains("track")) track = intent.getExtras().get(key).toString();
			}
			
			sendRemoteNotification(context, context.getResources().getString(R.string.sbr_now_playing), artist + " - " + track);
		}
	}
	
	/**
	 * Sends a BroadcastIntent to Remote Notifier
	 * 
	 * @see http://code.google.com/p/android-notifier/
	 * @see http://code.google.com/p/android-notifier/wiki/ThirdPartyNotifications
	 */
	private void sendRemoteNotification(Context context, String title, String description) {
		
		Log.i(TAG, "Sending notification: " + title + "\n" + description);
		
		Intent i = new Intent("org.damazio.notifier.service.UserReceiver.USER_MESSAGE");
		i.putExtra("title", title);
		i.putExtra("description", description);
		context.sendBroadcast(i);
	}
}